import {
	CREATE_POST,
	GET_POST,
	REMOVE_POST,
	GET_POSTS,
	POST_ERROR,
} from '../actions/types';

const initialState = {
	post: null,
	posts: [],
	error: {},
	loading: true,
};

export default (state = initialState, action) => {
	const { type, payload } = action;

	switch (type) {
		case GET_POSTS:
			return {
				...state,
				posts: payload,
				loading: false,
			};
		case POST_ERROR:
			return {
				...state,
				error: payload,
				loading: false,
			};
		default:
			return {
				...state,
			};
	}
};
