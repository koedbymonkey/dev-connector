import axios from 'axios';
import { setAlert } from './alert';
import { GET_POST, GET_POSTS, POST_ERROR } from './types';

export const createPost = (formData, history, edit = false) => async (
	dispatch
) => {
	try {
		const config = {
			headers: {
				'Content-Type': 'application/json',
			},
		};

		await axios.post('/api/posts', formData, config);

		dispatch(getPosts());
		dispatch(setAlert(edit ? 'Post Updated' : 'Post Created', 'success'));

		history.push('/posts');
	} catch (err) {
		const errors = err.response;
		if (errors) {
			errors.forEach((error) => {
				dispatch(setAlert(error.msg, 'danger'));
			});
		}
		dispatch({
			type: POST_ERROR,
			payload: { msg: err.message, status: err.response },
		});
	}
};

export const getPosts = () => async (dispatch) => {
	try {
		const res = await axios.get('/api/posts');

		dispatch({
			type: GET_POSTS,
			payload: res.data,
		});
	} catch (err) {
		dispatch({
			type: POST_ERROR,
			payload: { msg: err.message, status: err.response.status },
		});
	}
};

export const removePost = (id) => async (dispatch) => {
	try {
		await axios.delete(`/api/posts/${id}`);
		dispatch(setAlert('Post Removed', 'success'));
		dispatch(getPosts());
	} catch (err) {
		const error = err.response.data.msg;
		if (error) {
			dispatch(setAlert(error, 'danger'));
		}
		dispatch({
			type: POST_ERROR,
			payload: { msg: err.message, status: err.response },
		});
	}
};
