import React, { Fragment } from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logout } from '../../actions/auth';

const Navbar = ({ auth: { isAuthenticated, loading }, logout }) => {
	const authLinks = (
		<ul>
			<li>
				<Link to='/profiles'>
					<span className='hide-sm'>Developers</span>{' '}
					<span className='text-primary'>
						<i className='fa fa-users'></i>
					</span>
				</Link>
			</li>
			<li>
				<Link to='/dashboard'>
					<span className='hide-sm'>Dashboard</span>{' '}
					<span className='text-success'>
						<i className='fa fa-user'></i>
					</span>
				</Link>
			</li>
			<li>
				<Link onClick={logout} to='/login'>
					<span className='hide-sm'>Logout</span>{' '}
					<span className='text-danger'>
						<i className='fa fa-sign-out'></i>
					</span>
				</Link>
			</li>
		</ul>
	);

	const guestLinks = (
		<ul>
			<li>
				<Link to='/profiles'>
					<span className='hide-sm'>Developers</span>{' '}
					<span className='text-primary'>
						<i className='fa fa-users'></i>
					</span>
				</Link>
			</li>
			<li>
				<Link to='/register'>
					<span className='hide-sm'>Register</span>{' '}
					<span className='text-secondary'>
						<i className='fa fa-user-plus'></i>
					</span>
				</Link>
			</li>
			<li>
				<Link to='/login'>
					<span className='hide-sm'>Login</span>{' '}
					<span className='text-success'>
						<i className='fa fa-sign-in'></i>
					</span>
				</Link>
			</li>
		</ul>
	);

	return (
		<nav className='navbar bg-dark'>
			<h1>
				<Link to='/'>
					<i className='fa fa-code'></i> DevConnector
				</Link>
			</h1>
			{!loading && (
				<Fragment>{isAuthenticated ? authLinks : guestLinks}</Fragment>
			)}
		</nav>
	);
};

Navbar.propTypes = {
	logout: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
	auth: state.auth,
});

export default connect(mapStateToProps, { logout })(Navbar);
