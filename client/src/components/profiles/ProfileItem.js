import React from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

const ProfileItem = ({
	profile: {
		user: { name, avatar },
		status,
		company,
		location,
		skills,
		_id,
	},
}) => {
	return (
		<div className='profile bg-dark r'>
			<img className='round-img' src={avatar} alt='' />
			<div>
				<h2>{name}</h2>
				<p>
					{status} {company && <span>at {company}</span>}
				</p>
				<p>{location && <span>{location}</span>}</p>
				<Link to={`/profile/${_id}`} className='btn btn-primary'>
					View Profile
				</Link>
			</div>

			<ul>
				{skills.length !== 0 &&
					skills.slice(0, 5).map((skill, index) => (
						<li key={index} className='text-primary'>
							<i className='fa fa-check'></i> {skill}
						</li>
					))}
			</ul>
		</div>
	);
};

ProfileItem.propTypes = {
	profile: PropTypes.object.isRequired,
};

export default ProfileItem;
