import React from 'react';
import Moment from 'react-moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { removeEducation } from '../../actions/profile';

const EducationItem = ({
	removeEducation,
	education: { school, degree, from, to, current, _id },
}) => {
	return (
		<tr>
			<td>{school}</td>
			<td className='hide-sm'>{degree}</td>
			{!current ? (
				<td className='hide-sm'>
					<Moment duration={from}>{to}</Moment>
				</td>
			) : (
				<td>
					<strong>Current School: </strong>
					<Moment date={from} durationFromNow />
				</td>
			)}
			<td>
				<button
					id={_id}
					className='btn btn-danger'
					onClick={(e) => removeEducation(e.target.id)}
				>
					Delete
				</button>
			</td>
		</tr>
	);
};

EducationItem.propTypes = {
	education: PropTypes.object.isRequired,
	removeEducation: PropTypes.func.isRequired,
};

export default connect(null, { removeEducation })(EducationItem);
