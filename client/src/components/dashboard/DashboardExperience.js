import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import ExperienceItem from './ExperienceItem';

const DashboardExperience = ({ experience }) => {
	return (
		<Fragment>
			<h2 className='my-2'>Experience Credentials</h2>
			<table className='table'>
				<thead>
					<tr>
						<th>Company</th>
						<th className='hide-sm'>Title</th>
						<th className='hide-sm'>Time</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					{experience.map((experience) => (
						<ExperienceItem key={experience._id} experience={experience} />
					))}
				</tbody>
			</table>
		</Fragment>
	);
};

const mapStateToProps = (state) => ({
	experience: state.profile.profile.experience,
});

export default connect(mapStateToProps)(DashboardExperience);
