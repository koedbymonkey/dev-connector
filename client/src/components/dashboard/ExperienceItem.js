import React from 'react';
import Moment from 'react-moment';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { removeExperience } from '../../actions/profile';

const ExperienceItem = ({
	removeExperience,
	experience: { title, company, from, to, current, _id },
}) => {
	return (
		<tr>
			<td>{company}</td>
			<td className='hide-sm'>{title}</td>
			{!current ? (
				<td className='hide-sm'>
					<Moment duration={from}>{to}</Moment>
				</td>
			) : (
				<td>
					<strong>Current Job: </strong>
					<Moment date={from} durationFromNow />
				</td>
			)}
			<td>
				<button
					id={_id}
					className='btn btn-danger'
					onClick={(e) => removeExperience(e.target.id)}
				>
					Delete
				</button>
			</td>
		</tr>
	);
};

ExperienceItem.propTypes = {
	experience: PropTypes.object.isRequired,
	removeExperience: PropTypes.func.isRequired,
};

export default connect(null, { removeExperience })(ExperienceItem);
