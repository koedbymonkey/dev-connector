import React, { useEffect, Fragment } from 'react';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';
import Spinner from '../layout/Spinner';
import { connect } from 'react-redux';
import { getCurrentProfile, deleteAccount } from '../../actions/profile';
import DashboardActions from './DashboardActions';
import DashboardExperience from './DashboardExperience';
import DashboardEducation from './DashboardEducation';

const Dashboard = ({
	getCurrentProfile,
	deleteAccount,
	auth,
	profile: { profile, loading },
}) => {
	useEffect(() => {
		getCurrentProfile();
	}, [getCurrentProfile]);

	return loading && profile === null ? (
		<Spinner />
	) : (
		<Fragment>
			<section className='container'>
				<h1 className='large text-primary'>Dashboard</h1>
				<p className='lead'>
					<i className='fa fa-user'></i>{' '}
					{auth.user && `Welcome ${auth.user.name}`}
				</p>
				{profile !== null ? (
					<Fragment>
						<DashboardActions />
						{profile.experience.length !== 0 ? (
							<DashboardExperience />
						) : (
							<div className='card my-3'>No Experience Added</div>
						)}
						{profile.education.length !== 0 ? (
							<DashboardEducation />
						) : (
							<div className='card my-3'>No Education Added</div>
						)}
						<div className='my-2'>
							<button className='btn btn-danger' onClick={deleteAccount}>
								<i className='fa fa-user'></i> Delete My Account
							</button>
						</div>
					</Fragment>
				) : (
					<Fragment>
						<p>You have not yet setup a profile, please add some info</p>
						<Link to='/edit-profile' className='btn btn-primary my-1'>
							Create Profile
						</Link>
					</Fragment>
				)}
			</section>
		</Fragment>
	);
};

Dashboard.propTypes = {
	getCurrentProfile: PropTypes.func.isRequired,
	auth: PropTypes.object.isRequired,
	profile: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
	auth: state.auth,
	profile: state.profile,
	getCurrentProfile: PropTypes.func.isRequired,
	deleteAccount: PropTypes.func.isRequired,
});

export default connect(mapStateToProps, { getCurrentProfile, deleteAccount })(
	Dashboard
);
