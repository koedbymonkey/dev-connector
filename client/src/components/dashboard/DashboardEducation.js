import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import EducationItem from './EducationItem';

const DashboardEducation = ({ education }) => {
	return (
		<Fragment>
			<h2 className='my-2'>Education Credentials</h2>
			<table className='table'>
				<thead>
					<tr>
						<th>School</th>
						<th className='hide-sm'>Degree</th>
						<th className='hide-sm'>Time</th>
						<th />
					</tr>
				</thead>
				<tbody>
					{education.map((education) => (
						<EducationItem key={education._id} education={education} />
					))}
				</tbody>
			</table>
		</Fragment>
	);
};

const mapStateToProps = (state) => ({
	education: state.profile.profile.education,
});

export default connect(mapStateToProps)(DashboardEducation);
