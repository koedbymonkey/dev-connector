import React, { Fragment } from 'react';
import Moment from 'react-moment';
import PropTypes from 'prop-types';

const ProfileExperience = ({ profile: { experience } }) => {
	return (
		<Fragment>
			{experience.length !== 0 && (
				<div className='profile-exp bg-white p-2 r'>
					<h2 className='text-primary'>Experience</h2>
					{experience.map((exp, index) => (
						<div key={index}>
							<h3 className='text-dark'>{exp.company}</h3>
							{exp.current ? (
								<span>
									<Moment format='MMM YYYY'>{exp.from}</Moment> - Now
								</span>
							) : (
								<span>
									<Moment format='MMM YYYY'>{exp.from}</Moment> -{' '}
									<Moment format='MMM YYYY'>{exp.to}</Moment>
								</span>
							)}
							<p>
								<strong>Position: </strong>
								{exp.title}
							</p>
							<p>
								<strong>Description: </strong>
								{exp.description}
							</p>
						</div>
					))}
				</div>
			)}
		</Fragment>
	);
};

ProfileExperience.propTypes = {
	profile: PropTypes.object.isRequired,
};

export default ProfileExperience;
