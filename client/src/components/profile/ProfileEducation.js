import React, { Fragment } from 'react';
import Moment from 'react-moment';
import PropTypes from 'prop-types';

const ProfileEducation = ({ profile: { education } }) => {
	return (
		<Fragment>
			{education.length !== 0 && (
				<div className='profile-edu bg-white p-2 r'>
					<h2 className='text-primary'>Education</h2>
					{education.map((edu, index) => (
						<div key={index}>
							<h3 className='text-dark'>{edu.school}</h3>
							{edu.current ? (
								<span>
									<Moment format='MMM YYYY'>{edu.from}</Moment> - Current
								</span>
							) : (
								<span>
									<Moment format='MMM YYYY'>{edu.from}</Moment> -{' '}
									<Moment format='MMM YYYY'>{edu.to}</Moment>
								</span>
							)}
							<p>
								<strong>Position: </strong>
								{edu.title}
							</p>
							<p>
								<strong>Description: </strong>
								{edu.description}
							</p>
						</div>
					))}
				</div>
			)}
		</Fragment>
	);
};

ProfileEducation.propTypes = {
	profile: PropTypes.object.isRequired,
};

export default ProfileEducation;
