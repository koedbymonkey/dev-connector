import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { getGithubRepos } from '../../actions/profile';
import PropTypes from 'prop-types';

const ProfileRepos = ({ username, repos, getGithubRepos }) => {
	useEffect(() => {
		getGithubRepos(username);
	}, [getGithubRepos, username]);

	return (
		<Fragment>
			{repos.length !== 0 && (
				<div className='profile-github'>
					<h2 className='text-primary my-1'>
						<i className='fa fa-github'></i> Github Repos
					</h2>
					{repos.map((repo) => (
						<div key={repo._id} className='repo bg-white p-1 my-1 r'>
							<div>
								<h4>
									<a
										href={repo.html_url}
										target='_blank'
										rel='noopener noreferrer'
									>
										{repo.name}
									</a>
								</h4>
								<p>{repo.description}</p>
							</div>
							<div>
								<ul>
									<li className='badge badge-primary'>
										Stars: {repo.stargazers_count}
									</li>
									<li className='badge badge-dark'>
										Watchers: {repo.watchers}
									</li>
									<li className='badge badge-light'>Forks: {repo.forks}</li>
								</ul>
							</div>
						</div>
					))}
				</div>
			)}
		</Fragment>
	);
};

ProfileRepos.propTypes = {
	username: PropTypes.string.isRequired,
	repos: PropTypes.array.isRequired,
	getGithubRepos: PropTypes.func.isRequired,
};

export default connect(null, { getGithubRepos })(ProfileRepos);
