import React, { Fragment, useEffect } from 'react';
import { connect } from 'react-redux';
import { getPosts } from '../../actions/post';
import Spinner from '../layout/Spinner';
import CreatePost from './CreatePost';
import PostList from './PostList';
import PropTypes from 'prop-types';

const Posts = ({ posts, loading, getPosts }) => {
	useEffect(() => {
		getPosts();
	}, [getPosts]);

	return (
		<Fragment>
			{loading ? (
				<Spinner />
			) : (
				<Fragment>
					<h1 className='large text-primary'>Posts</h1>
					<p className='lead'>
						<i className='fa fa-user'></i> Welcome to the community!
					</p>
					<CreatePost />
					<PostList posts={posts} />
				</Fragment>
			)}
		</Fragment>
	);
};

Posts.propTypes = {
	posts: PropTypes.array.isRequired,
	loading: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
	posts: state.post.posts,
	loading: state.post.loading,
});

export default connect(mapStateToProps, { getPosts })(Posts);
