import React from 'react';
import PostItem from './PostItem';
import PropTypes from 'prop-types';

const PostList = ({ posts }) => {
	return (
		<div className='posts'>
			{posts.map((post) => (
				<PostItem key={post._id} post={post} />
			))}
		</div>
	);
};

PostList.propTypes = {
	posts: PropTypes.array.isRequired,
};

export default PostList;
