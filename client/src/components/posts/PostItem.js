import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { removePost } from '../../actions/post';
import Moment from 'react-moment';
import PropTypes from 'prop-types';

const PostItem = ({ post, removePost }) => {
	const onClick = () => {
		removePost(post._id);
	};

	return (
		<div className='post bg-white p-1 my-1 r'>
			<div>
				<Link to='/profile'>
					<img className='round-img' src={post.avatar} alt='' />
					<h4>{post.name}</h4>
				</Link>
			</div>
			<div>
				<p className='my-1'>{post.text}</p>
				<p className='post-date'>
					Posted on <Moment format='MM/DD/YYYY'>{post.date}</Moment>
				</p>
				<button type='button' className='btn btn-light'>
					<i className='fa fa-thumbs-up'></i>
					<span> {post.likes.length}</span>
				</button>
				<button type='button' className='btn btn-light'>
					<i className='fa fa-thumbs-down'></i>
				</button>
				<Link to='/post/:id' className='btn btn-primary'>
					Discussion{' '}
					<span className='comment-count'>{post.comments.length}</span>
				</Link>
				<button type='button' className='btn btn-danger' onClick={onClick}>
					<i className='fa fa-times'></i>
				</button>
			</div>
		</div>
	);
};

PostItem.propTypes = {
	post: PropTypes.object.isRequired,
	removePost: PropTypes.func.isRequired,
};

export default connect(null, { removePost })(PostItem);
