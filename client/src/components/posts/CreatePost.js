import React, { useState } from 'react';
import { connect } from 'react-redux';
import { createPost } from '../../actions/post';
import PropTypes from 'prop-types';

const CreatePost = ({ createPost }) => {
	const [formData, setFormData] = useState({
		text: '',
	});

	const { text } = formData;

	const onChange = (e) =>
		setFormData({ ...formData, [e.target.name]: e.target.value });

	const onSubmit = (e) => {
		e.preventDefault();
		createPost(formData);
		setFormData({ ...formData, text: '' });
	};

	return (
		<div className='post-form'>
			<div className='bg-primary p r'>
				<h3>Say Something...</h3>
			</div>
			<form onSubmit={onSubmit} className='form my-1'>
				<textarea
					name='text'
					cols='30'
					rows='5'
					placeholder='Create a post'
					value={text}
					required
					onChange={(e) => onChange(e)}
				></textarea>
				<input type='submit' className='btn btn-dark my-1' value='Submit' />
			</form>
		</div>
	);
};

CreatePost.propTypes = {
	createPost: PropTypes.func.isRequired,
};

export default connect(null, { createPost })(CreatePost);
