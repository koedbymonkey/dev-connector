const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const Profile = require('../../models/Profile');
const User = require('../../models/User');
const Post = require('../../models/Post');
const axios = require('axios');
const config = require('config');
const { check, validationResult } = require('express-validator');
const { json } = require('express');

// @route     GET api/profile/myprofile
// @desc      Get Logged In Users Profile
// @access    Private
router.get('/myprofile', auth, async (req, res) => {
	try {
		const profile = await Profile.findOne({
			user: req.user.id,
		}).populate('user', ['name', 'avatar']);
		if (!profile)
			return res.status(400).json({ msg: 'There is no profile for this user' });
		res.json(profile);
	} catch (err) {
		console.error(err.message);
		res.status(500).send('Server Error');
	}
});

// @route     POST api/profile
// @desc      Create or update user profile
// @access    Private
router.post(
	'/',
	[
		auth,
		[
			check('status', 'Status is required').notEmpty(),
			check('skills', 'Skills is required').notEmpty(),
		],
	],
	async (req, res) => {
		const errors = validationResult(req);
		const re = RegExp(/^(http|https):\/\//i);
		const str = 'https://';
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}
		const {
			company,
			website,
			location,
			bio,
			status,
			githubusername,
			skills,
			youtube,
			facebook,
			twitter,
			instagram,
			linkedin,
		} = req.body;

		const profileFields = {};
		profileFields.user = req.user.id;
		if (company) profileFields.company = company;
		if (website) {
			if (re.test(website) === false) {
				profileFields.website = str.concat(website);
			} else {
				profileFields.website = website;
			}
		}
		if (location) profileFields.location = location;
		if (bio) profileFields.bio = bio;
		if (status) profileFields.status = status;
		if (githubusername) profileFields.githubusername = githubusername;
		if (skills) {
			profileFields.skills = skills.split(',').map((skill) => skill.trim());
		}

		profileFields.social = {};
		if (youtube) {
			if (re.test(youtube) === false) {
				profileFields.social.youtube = str.concat(youtube);
			} else {
				profileFields.social.youtube = youtube;
			}
		}
		if (twitter) {
			if (re.test(twitter) === false) {
				profileFields.social.twitter = str.concat(twitter);
			} else {
				profileFields.social.twitter = twitter;
			}
		}
		if (facebook) {
			if (re.test(facebook) === false) {
				profileFields.social.facebook = str.concat(facebook);
			} else {
				profileFields.social.facebook = facebook;
			}
		}
		if (linkedin) {
			if (re.test(linkedin) === false) {
				profileFields.social.linkedin = str.concat(linkedin);
			} else {
				profileFields.social.linkedin = linkedin;
			}
		}
		if (instagram) {
			if (re.test(instagram) === false) {
				profileFields.social.instagram = str.concat(instagram);
			} else {
				profileFields.social.instagram = instagram;
			}
		}

		try {
			let profile = await Profile.findOne({ user: req.user.id });
			if (profile) {
				profile = await Profile.findOneAndUpdate(
					{ user: req.user.id },
					{ $set: profileFields },
					{ new: true }
				);

				return res.json(profile);
			}

			profile = new Profile(profileFields);
			await profile.save();
			res.json(profile);
		} catch (err) {
			console.error(err.message);
			res.status(500).send('Server Error');
		}
	}
);

// @route     GET api/profile
// @desc      Get all profiles
// @access    Public
router.get('/', async (req, res) => {
	try {
		const profiles = await Profile.find().populate('user', ['name', 'avatar']);
		res.json(profiles);
	} catch (err) {
		console.error(err.message);
		res.status(500).send('Server Error');
	}
});

// @route     GET api/profile/:id
// @desc      Get profile by ID
// @access    Public
router.get('/:id', async (req, res) => {
	try {
		const profile = await Profile.findOne({
			_id: req.params.id,
		}).populate('user', ['name', 'avatar']);

		if (!profile) return res.status(400).json({ msg: 'Profile not found' });

		res.json(profile);
	} catch (err) {
		console.error(err.message);
		if (err.kind == 'ObjectId')
			return res.status(400).json({ msg: 'Profile not found' });
		res.status(500).send('Server Error');
	}
});

// @route     DELETE api/profile
// @desc      Delete profile, user and posts
// @access    Private
router.delete('/', auth, async (req, res) => {
	try {
		await Post.deleteMany({ user: req.user.id });
		await Profile.findOneAndRemove({ user: req.user.id });
		await User.findOneAndRemove({ _id: req.user.id });

		res.json({ msg: 'User deleted' });
	} catch (err) {
		console.error(err.message);
		res.status(500).send('Server Error');
	}
});

// @route     PUT api/profile/experience
// @desc      Add profile experience
// @access    Private
router.put(
	'/experience',
	[
		auth,
		[
			check('title', 'Title is required').notEmpty(),
			check('company', 'Company is required').notEmpty(),
			check('from', 'From date is required').notEmpty(),
		],
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty())
			return res.status(400).json({ errors: errors.array() });

		const {
			title,
			company,
			location,
			from,
			to,
			current,
			description,
		} = req.body;

		const newExp = {
			title,
			company,
			location,
			from,
			to,
			current,
			description,
		};

		try {
			const profile = await Profile.findOne({ user: req.user.id });
			profile.experience.unshift(newExp);
			await profile.save();

			res.json(profile);
		} catch (err) {
			console.error(err.message);
			res.status(500).send('Server Error');
		}
	}
);

// @route     DELETE api/profile/experience/:exp_id
// @desc      Delete experience from profile
// @access    Private
router.delete('/experience/:exp_id', auth, async (req, res) => {
	try {
		const foundProfile = await Profile.findOne({ user: req.user.id });

		foundProfile.experience = foundProfile.experience.filter(
			(exp) => exp._id.toString() !== req.params.exp_id
		);

		await foundProfile.save();
		return res.status(200).json(foundProfile);
	} catch (error) {
		console.error(error);
		return res.status(500).send({ msg: 'Server error' });
	}
});

// @route     PUT api/profile/education
// @desc      Add profile education
// @access    Private
router.put(
	'/education',
	[
		auth,
		[
			check('school', 'School is required').notEmpty(),
			check('degree', 'Degree is required').notEmpty(),
			check('from', 'From date is required').notEmpty(),
			check('fieldofstudy', 'Field of study is required').notEmpty(),
		],
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty())
			return res.status(400).json({ errors: errors.array() });

		const {
			school,
			degree,
			fieldofstudy,
			from,
			to,
			current,
			description,
		} = req.body;

		const newEdu = {
			school,
			degree,
			fieldofstudy,
			from,
			to,
			current,
			description,
		};

		try {
			const profile = await Profile.findOne({ user: req.user.id });
			profile.education.unshift(newEdu);
			await profile.save();

			res.json(profile);
		} catch (err) {
			console.error(err.message);
			res.status(500).send('Server Error');
		}
	}
);

// @route     DELETE api/profile/education/:edu_id
// @desc      Delete education from profile
// @access    Private
router.delete('/education/:edu_id', auth, async (req, res) => {
	try {
		const foundProfile = await Profile.findOne({ user: req.user.id });

		foundProfile.education = foundProfile.education.filter(
			(edu) => edu._id.toString() !== req.params.edu_id
		);

		await foundProfile.save();
		return res.status(200).json(foundProfile);
	} catch (error) {
		console.error(error);
		return res.status(500).json({ msg: 'Server error' });
	}
});

// @route     GET api/profile/github/:username
// @desc      Get user repos from github
// @access    Public
router.get('/github/:username', async (req, res) => {
	try {
		const repos = await axios.get(
			`https://api.github.com/users/${
				req.params.username
			}/repos?per_page=5&sort=created:asc&client_id=${config.get(
				'GITHUB_CLIENT_ID'
			)}&client_secret=${config.get('GITHUB_SECRET')}`
		);
		res.json(repos.data);
	} catch (err) {
		if (res.statusCode !== 500)
			return res.status(404).json({ msg: 'No Github user found' });
		console.error(err.message);
		res.status(500).send('Server Error');
	}
});

module.exports = router;
