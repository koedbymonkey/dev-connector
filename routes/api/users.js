const express = require('express');
const router = express.Router();
const gravatar = require('gravatar');
const { check, validationResult } = require('express-validator');
const bcrypt = require('bcryptjs');
const config = require('config');
const jwt = require('jsonwebtoken');
const User = require('../../models/User');

// @route     POST api/users
// @desc      Register User
// @access    Public
router.post(
	'/',
	//Validation
	[
		check('name', 'Name is required').notEmpty(),
		check('email', 'Please include a valid email').isEmail(),
		check(
			'password',
			'Please enter a password with 6 or more characters'
		).isLength({ min: 6 }),
	],
	async (req, res) => {
		const errors = validationResult(req);
		if (!errors.isEmpty()) {
			return res.status(400).json({ errors: errors.array() });
		}

		//Desructure
		const { name, email, password } = req.body;

		try {
			//See if user already exists
			let user = await User.findOne({ email });
			if (user) {
				return res
					.status(400)
					.json({ errors: [{ msg: 'User already exists' }] });
			}
			//Get users gravatar
			const avatar = gravatar.url(email, {
				s: '200',
				r: 'pg',
				d: 'mm',
			});
			//Create new instance of a User
			user = new User({
				name,
				email,
				avatar,
				password,
			});
			//Encrypt password
			const salt = await bcrypt.genSalt(11);
			user.password = await bcrypt.hash(password, salt);
			//Save User to DB
			await user.save();
			//Return jsonwebtoken
			const payload = {
				user: {
					id: user.id,
				},
			};
			//Create jsonwebtoken and send it to the user
			jwt.sign(
				payload,
				config.get('jwtSecret'),
				{ expiresIn: 3600 },
				(err, token) => {
					if (err) throw err;
					res.json({ token });
				}
			);
		} catch (err) {
			console.error(err.message);
			res.status(500).send('Server Error');
		}
	}
);

module.exports = router;
